# Reports with R RStudio and LaTeX

A template for the creation of reports using R, RStudio, and LaTeX

This project comprises a starting point for the creation of reports using R for 
data analysis with the RStudio IDE and LaTeX. Most settings are controlled by 
the r-clinic_scrreprt.sty file.

The project to give anyone with the intention to learn about the co-integration
of R, RStudio, and LaTeX a headstart and starting point, to create own reports.


## Notice

(c) Dirk Caspary 2018

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software, associated documentation files, and source code 
(the "Software"), to deal in the Software without restriction, including without
limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom 
the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

The software is provided "as is", without warranty of any kind, express
or implied, including but not limited to the warranties of merchantability,
fitness for a particular purpose and noninfringement. In no event shall
the authors or copyright holders be liable for any claim, damages or other
liability, whether in an action of contract, tort or otherwise, arising
from, out of or in connection with the software or the use or other dealings
in the software.