%% The R-Clinic style file 
%%
%% Copyright 2018 Dirk Caspary
%%
%% Permission is hereby granted, free of charge, to any person obtaining a 
%% copy of this software and associated documentation files (the "Software"),
%% to deal in the Software without restriction, including without limitation
%% the rights to use, copy, modify, merge, publish, distribute, sublicense,
%% and/or sell copies of the Software, and to permit persons to whom the
%% Software is furnished to do so, subject to the following conditions:
%% 
%% The above copyright notice and this permission notice shall be included
%% in all copies or substantial portions of the Software.
%% 
%% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
%% OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
%% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
%% THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
%% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
%% FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
%% IN THE SOFTWARE.
%%
%% The style file should be in placed in 
%% $TEXMFHOME/tex/latex/r-clinic_scrreprt/
%% and run texhash $TEXMFHOME
%%
%% The logo pdf-file should be placed in
%% $TEXMFHOME/tex/generic/images/
%%
%% TeXStudio appears to ignore $TEXMFHOME, so start TeXStudio from the console

\NeedsTeXFormat{LaTeX2e}[1994/06/01]
\ProvidesPackage{r-clinic_scrreprt}[2018/11/25 R-Clinic style for Scrreprt]

%% -----------------------------------------------------------------------------
%% Packages
\RequirePackage{scrhack} %% to fix the "\float@addtolists detected!(scrreprt)" warning
\RequirePackage[table]{xcolor}
\RequirePackage{graphicx}
\graphicspath{{./figures/}}

\RequirePackage[T1]{fontenc}
\RequirePackage[utf8]{inputenc}
\RequirePackage{times}
\RequirePackage{pslatex}
\RequirePackage[english]{babel}
\RequirePackage{tocbibind}
\RequirePackage{setspace} %% line spacing
\RequirePackage{longtable} %% longtable before
\RequirePackage{float} % for the H option for floating objects
\RequirePackage{lipsum} %% dummy text
\RequirePackage[loose]{units} %% nice dimensions and units

\RequirePackage{scrpage2} % page headers and footers; scrpage2 is replaced by scrlayer-scrpage
%\RequirePackage{scrlayer-scrpage} % page headers and footers
\setheadtopline{0.0pt} %% header top line
\setheadsepline{0.4pt} %% header btm line
\setfootsepline{0.4pt} %% header btm line
% headings on the first page of a chapter -- ugly but standard
% \renewcommand*{\chapterpagestyle}{scrheadings}
% footer on the first page of a chapter -- ugly but standard
% \renewcommand*{\chapterpagestyle}{scrfooter} %% TODO: causes error messages!
%% Header and footer setting for class option "oneside"
\ihead[]{\reportTitle}
\chead[]{}
\ohead[]{Project-ID~\projectID}

\ofoot[\pagemark]{\pagemark}
\ifoot[]{\href{https://dirk.gitlab.io/r-clinic}{{The~\includegraphics[height=1.67ex,keepaspectratio=true]{logo}}-Clinic}}
\cfoot[]{}

%% Header and footer setting for class option "twoside"
% \clearscrheadfoot
% \lehead[scrplain-left-even]{scrheadings-left-even}
% \cehead[scrplain-centre-even]{scrheadings-centre-even}
% \rehead[scrplain-right-even]{scrheadings-right-even}
% \lefoot[scrplain-left-even]{scrheadings-left-even}
% \cefoot[scrplain-centre-even]{scrheadings-centre-even}
% \refoot[scrplain-right-even {scrheadings-right-even}

% \lohead[scrplain-left-odd]{scrheadings-left-odd}
% \cohead[scrplain-centre-odd]{scrheadings-centre-odd}
% \rohead[scrplain-right-odd]{scrheadings-right-odd}
% \lofoot[scrplain-left-odd]{scrheadings-left-odd }
% \cofoot[scrplain-centre-odd]{scrheadings-centre-odd }
% \rofoot[scrplain-right-odd]{scrheadings-right-odd}

\RequirePackage{hyperref} %% for hyperlinks
\hypersetup{
   pdfpagemode=UseNone,
   colorlinks=true,
   linkcolor=blue,
   citecolor=red,
   breaklinks=true,
   pdfstartpage=1,
   pdfstartview=FitB
}

\RequirePackage{lineno} %% settings after the table of contents

\RequirePackage[nodayofweek]{datetime} %% getting better access to date formats
\newdateformat{titledate}{\monthname[\THEMONTH] \twodigit{\THEDAY}, \THEYEAR}
\newdateformat{secondpagedate}{\monthname[\THEMONTH]{ }\THEYEAR}
\newdateformat{theyear}{\THEYEAR}

\RequirePackage{caption}  %% caption formatting
\captionsetup{format=hang,margin=10pt,font=small,labelfont=bf}


\RequirePackage{booktabs}
\RequirePackage{longtable}
\RequirePackage{array}
\RequirePackage{multirow}
\RequirePackage[table]{xcolor}
\RequirePackage{wrapfig}
\RequirePackage{float}
\RequirePackage{colortbl}
\RequirePackage{pdflscape}
\RequirePackage{tabu}
\RequirePackage{threeparttable}
\RequirePackage{threeparttablex}
\RequirePackage[normalem]{ulem}
\RequirePackage{makecell}

%% #############################################################################
%% second page
\def\secondpage{\clearpage%\null\vfill
   \begin{minipage}[b]{1.0\textwidth}
      \footnotesize
      \setlength{\parskip}{0.5\baselineskip}
      Project-ID: \projectID
   \end{minipage}
   \null\vfill
   \pagestyle{empty}
   \footnotesize
   \textbf{Notice\\}
   
   \copyright~\theyear\today~Dirk~Caspary
   
   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   The software is provided "as is", without warranty of any kind, express or 
   implied, including but not limited to the warranties of merchantability, 
   fitness for a particular purpose and noninfringement. In no event shall
   the authors or copyright holders be liable for any claim, damages or other
   liability, whether in an action of contract, tort or otherwise, arising
   from, out of or in connection with the software or the use or other
   dealings in the software.

   \rule{0pt}{5ex}\secondpagedate\today\hfill\copyright\,\theyear\today~Dirk~Caspary
   \cleardoublepage
   \normalsize
}
\makeatletter
\g@addto@macro{\maketitle}{\secondpage}
\makeatother

%% #############################################################################

\renewcommand{\topfraction}{1.0}
\renewcommand{\bottomfraction}{1.0}

\setlength\parskip{1ex plus 0.2ex minus 0.1 ex}
\setlength{\parindent}{0pt}

\setcounter{tocdepth}{2}

\newcommand{\textem}[1]{\textit{#1}}
\newcommand{\textmarker}[1]{\colorbox{yellow}{#1}}

\endinput